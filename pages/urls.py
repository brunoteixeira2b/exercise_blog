from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.index, name='index'),
    path('article/<int:pk>/', views.article_detail, name='article_detail'),
    path('article/<int:pk>/comment/', views.add_comment_to_article, name='add_comment_to_article'),
    path('categories/<slug>/', views.category_page, name='category_page'),
    path('tags/<slug>/', views.tag_page, name='tag_page'),
]