from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.utils import timezone
from .models import Article, Category, Tag
from django.core.paginator import Paginator, PageNotAnInteger
from .forms import CommentForm

def index(request):
    articles = Article.objects.all()
    paginator = Paginator(articles, 5)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
	    articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)


    categories = Category.objects.all()
    tags = Tag.objects.all()

    context = {
        'articles': articles,
        'categories': categories,
        'tags': tags,
    }

    return render(request, 'pages/index.html', context)


def article_detail(request, pk):
    article = get_object_or_404(Article,pk=pk)
    return render(request, 'pages/article_detail.html', {'article': article})


def add_comment_to_article(request, pk):
    article = get_object_or_404(Article, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            # comment = form.save(commit=False)
            # comment.article = article
            # comment.save()
            form.save()
            return redirect('article_detail', pk=article.pk)
    else:
        form = CommentForm(initial={'article': article})

    return render(request, 'pages/add_comment_to_article.html', {'form': form})


def category_page(request, slug):
    category = Category.objects.get(slug=slug)
    tags = Tag.objects.all()
    # articles = Article.objects.filter(category=category)
    articles = category.articles.all()
    paginator = Paginator(articles, 5)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
	    articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)


    context = {
        'articles': articles,
        'category': category,
        'tags': tags,
    }

    return render(request, 'pages/category_page.html', context)


def tag_page(request, slug):
    category = Category.objects.all()
    tags = Tag.objects.get(slug=slug)
    # articles = Article.objects.filter(tags=tags)
    articles = tags.article_set.all()
    paginator = Paginator(articles, 5)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
	    articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)


    context = {
        'articles': articles,
        'category': category,
        'tags': tags,
    }

    return render(request, 'pages/tag_page.html', context)

   